<?php
/*
Plugin Name: Headlines News Manager
Plugin URI: https://bitbucket.org/riccimoore/headlines/
Description: Custom Post Types for listing and displaying external news and media references.
Author: Rob Moore
Version:0.0.1
Author URI: https://www.riccimoore.com
*/

add_action( 'init', 'themeprefix_create_custom_post_type' );

function themeprefix_create_custom_post_type() {

	$labels = array(
		'name'               => __( 'Headlines' ),
		'singular_name'      => __( 'Headline' ),
		'all_items'          => __( 'All Headlines' ),
		'add_new'            => _x( 'Add new Headline', 'Headlines' ),
		'add_new_item'       => __( 'Add new Headline' ),
		'edit_item'          => __( 'Edit Headline' ),
		'new_item'           => __( 'New Headline' ),
		'view_item'          => __( 'View Headline' ),
		'search_items'       => __( 'Search in Headlines' ),
		'not_found'          => __( 'No Headlines found' ),
		'not_found_in_trash' => __( 'No Headlines found in trash' ),
		'parent_item_colon'  => ''
	);
	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'has_archive'        => true,
		'menu_icon'          => 'dashicons-megaphone', //pick one here ~> https://developer.wordpress.org/resource/dashicons/
		'rewrite'            => array( 'slug' => 'press' ),
		'taxonomies'         => array( 'category', 'post_tag' ),
		'query_var'          => true,
		'menu_position'      => 5,
		'supports'           => array( 'thumbnail' , 'custom-fields', 'excerpt', 'comments', 'title', 'editor')
	);
	register_post_type( 'event', $args );
}

// flush the permalinks - ref - https://codex.wordpress.org/Function_Reference/register_post_type#Flushing_Rewrite_on_Activation

function themeprefix_my_rewrite_flush() {
    // First, we "add" the custom post type via the above written function.
    // Note: "add" is written with quotes, as CPTs don't get added to the DB,
    // They are only referenced in the post_type column with a post entry,
    // when you add a post of this CPT.
    themeprefix_create_custom_post_type();

    // ATTENTION: This is *only* done during plugin activation hook in this example!
    // You should *NEVER EVER* do this on every page load!!
    flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'themeprefix_my_rewrite_flush' );
